project = 'Pirate Radio DAO Community Documentation'
release = 'https://gitlab.com/CrunchBangDev/pirate-radio-dao.wiki.git'

extensions = ['myst_parser']
source_suffix = {'.md': 'markdown'}
source_parsers = {'.md': 'recommonmark.parser.CommonMarkParser'}
root_doc = 'index'
html_theme = 'alabaster'
