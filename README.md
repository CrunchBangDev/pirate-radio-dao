# What is this?
This is a community documentation project for the Pirate Radio [DAO](https://en.wikipedia.org/wiki/Decentralized_autonomous_organization).

The files in this repository are only for building the [Pirate Radio website](https://pirateradio.space).

Documentation contributions should be made to [the Wiki](/wiki) repository:
https://gitlab.com/CrunchBangDev/pirate-radio-dao.wiki.git